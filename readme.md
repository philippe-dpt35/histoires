# Histoires à reconstituer #

Cette application propose de reconstituer des histoires en images par glisser-déposer.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/histoires/index.html)

Illustration en fond d'application :

Page de couverture du livre "Où est passée Rainette ?" sous licence MIT

Aindri C.

http://www.litterature-jeunesse-libre.fr/bbs/titles/726
